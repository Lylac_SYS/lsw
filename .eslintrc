{
	"root": true,
	"env": {
		"browser": true,
		"node": true,
		"es6": true
	},
	"extends": [
		"eslint:recommended",
		"plugin:react/recommended",
		"plugin:react-hooks/recommended"
	],
	"parser": "@typescript-eslint/parser",
	"parserOptions": {
		"ecmaVersion": 2020,
		"sourceType": "module",
		"ecmaFeatures": {
			"jsx": true
		},
		"project": "tsconfig.json"
	},
	"settings": {
		"react": {
			"version": "detect"
		}
	},
	"plugins": [
		"@typescript-eslint",
		"react",
		"react-hooks"
	],
	"globals": {
		"React": "writable"
	},
	"rules": {
		"no-empty": "off",
		"no-unsafe-optional-chaining": "error",
		"no-useless-backreference": "warn",
		"require-atomic-updates": "warn",
		"curly": "error",
		"default-param-last": "error",
		"dot-location": [
			"error",
			"property"
		],
		"dot-notation": "error",
		"eqeqeq": [
			"error",
			"always"
		],
		"no-caller": "error",
		"no-case-declarations": "off",
		"no-constructor-return": "error",
		"no-else-return": "error",
		"no-extra-bind": "error",
		"no-floating-decimal": "error",
		"no-multi-spaces": "error",
		"no-octal-escape": "error",
		"no-proto": "error",
		"no-return-await": "error",
		"no-self-compare": "error",
		"no-sequences": "error",
		"no-throw-literal": "error",
		"no-useless-call": "error",
		"no-useless-concat": "error",
		"no-useless-return": "error",
		"no-void": "error",
		"prefer-regex-literals": "error",
		"no-unused-vars": [
			"warn",
			{
				"vars": "all",
				"args": "after-used"
			}
		],
		"array-bracket-newline": [
			"error",
			"consistent"
		],
		"array-bracket-spacing": [
			"error",
			"never"
		],
		"array-element-newline": [
			"error",
			"consistent"
		],
		"brace-style": [
			"error",
			"1tbs",
			{
				"allowSingleLine": false
			}
		],
		"comma-dangle": [
			"error",
			"never"
		],
		"comma-spacing": [
			"error",
			{
				"before": false,
				"after": true
			}
		],
		"comma-style": [
			"error",
			"last"
		],
		"computed-property-spacing": [
			"error",
			"never"
		],
		"eol-last": [
			"warn",
			"never"
		],
		"func-call-spacing": [
			"error",
			"never"
		],
		"func-style": [
			"error",
			"expression"
		],
		"function-call-argument-newline": [
			"error",
			"consistent"
		],
		"function-paren-newline": [
			"error",
			"multiline-arguments"
		],
		"indent": [
			"warn",
			"tab",
			{
				"SwitchCase": 1
			}
		],
		"jsx-quotes": [
			"error",
			"prefer-double"
		],
		"key-spacing": [
			"error",
			{
				"beforeColon": false,
				"afterColon": true,
				"mode": "strict"
			}
		],
		"keyword-spacing": [
			"error",
			{
				"before": true,
				"after": true
			}
		],
		"linebreak-style": [
			"error",
			"unix"
		],
		"lines-between-class-members": [
			"error",
			"always"
		],
		"max-statements-per-line": [
			"error",
			{
				"max": 1
			}
		],
		"new-parens": [
			"error",
			"always"
		],
		"no-array-constructor": "error",
		"no-lonely-if": "error",
		"no-multiple-empty-lines": [
			"warn",
			{
				"max": 1,
				"maxEOF": 0,
				"maxBOF": 0
			}
		],
		"no-negated-condition": "error",
		"no-new-object": "error",
		"no-trailing-spaces": [
			"error",
			{
				"skipBlankLines": true,
				"ignoreComments": true
			}
		],
		"no-unneeded-ternary": "error",
		"no-whitespace-before-property": "error",
		"object-curly-newline": [
			"error",
			{
				"consistent": true
			}
		],
		"object-curly-spacing": [
			"error",
			"always"
		],
		"object-property-newline": [
			"error",
			{
				"allowAllPropertiesOnSameLine": true
			}
		],
		"one-var": [
			"error",
			"never"
		],
		"operator-assignment": [
			"error",
			"always"
		],
		"operator-linebreak": [
			"error",
			"before"
		],
		"prefer-exponentiation-operator": "error",
		"prefer-object-spread": "error",
		"quote-props": [
			"error",
			"as-needed"
		],
		"quotes": [
			"error",
			"single",
			{
				"avoidEscape": true
			}
		],
		"semi": [
			"warn",
			"always"
		],
		"semi-spacing": [
			"error",
			{
				"before": false,
				"after": true
			}
		],
		"semi-style": [
			"error",
			"last"
		],
		"space-before-blocks": [
			"error",
			"always"
		],
		"space-before-function-paren": [
			"error",
			{
				"anonymous": "always",
				"named": "never",
				"asyncArrow": "always"
			}
		],
		"space-in-parens": [
			"error",
			"never"
		],
		"space-infix-ops": "error",
		"space-unary-ops": [
			"error",
			{
				"words": true,
				"nonwords": false
			}
		],
		"spaced-comment": [
			"warn",
			"always"
		],
		"switch-colon-spacing": [
			"error",
			{
				"after": true,
				"before": false
			}
		],
		"template-tag-spacing": [
			"error",
			"never"
		],
		"arrow-body-style": [
			"warn",
			"as-needed"
		],
		"arrow-parens": [
			"warn",
			"as-needed"
		],
		"arrow-spacing": [
			"error",
			{
				"before": true,
				"after": true
			}
		],
		"generator-star-spacing": [
			"error",
			{
				"before": false,
				"after": true
			}
		],
		"no-duplicate-imports": "error",
		"no-useless-computed-key": [
			"error",
			{
				"enforceForClassMembers": true
			}
		],
		"no-useless-constructor": "error",
		"no-useless-rename": "error",
		"no-var": "error",
		"object-shorthand": [
			"error",
			"always",
			{
				"avoidQuotes": true
			}
		],
		"prefer-arrow-callback": "error",
		"prefer-const": [
			"warn",
			{
				"destructuring": "all"
			}
		],
		"prefer-numeric-literals": "error",
		"prefer-rest-params": "error",
		"prefer-spread": "error",
		"prefer-template": "error",
		"rest-spread-spacing": [
			"error",
			"never"
		],
		"symbol-description": "error",
		"template-curly-spacing": [
			"error",
			"never"
		],
		"yield-star-spacing": [
			"error",
			"after"
		],
		"@typescript-eslint/prefer-for-of": "warn",
		"@typescript-eslint/prefer-includes": "error",
		"react/default-props-match-prop-types": [
			"error",
			{
				"allowRequiredDefaults": false
			}
		],
		"react/forbid-elements": [
			"error",
			{
				"forbid": [
					{
						"element": "a",
						"message": "use <Link> from '/components/Link' instead."
					}
				]
			}
		],
		"react/no-access-state-in-setstate": "error",
		"react/no-did-mount-set-state": "error",
		"react/no-did-update-set-state": "error",
		"react/no-redundant-should-component-update": "error",
		"react/no-unused-prop-types": "error",
		"react/no-unused-state": "error",
		"react/no-will-update-set-state": "error",
		"react/prefer-stateless-function": "error",
		"react/self-closing-comp": [
			"warn",
			{
				"component": true,
				"html": true
			}
		],
		"react/state-in-constructor": [
			"error",
			"always"
		],
		"react/static-property-placement": [
			"error",
			"static public field"
		],
		"react/style-prop-object": "error",
		"react/void-dom-elements-no-children": "error"
	},
	"overrides": [
		{
			"files": [
				"*.ts",
				"*.tsx"
			],
			"rules": {
				"no-undef": "off",
				"@typescript-eslint/adjacent-overload-signatures": "error",
				"@typescript-eslint/array-type": [
					"error",
					{
						"default": "array-simple"
					}
				],
				"@typescript-eslint/await-thenable": "error",
				"@typescript-eslint/ban-types": [
					"error",
					{
						"types": {
							"{}": false
						}
					}
				],
				"@typescript-eslint/class-literal-property-style": [
					"error",
					"fields"
				],
				"@typescript-eslint/consistent-indexed-object-style": [
					"error",
					"record"
				],
				"@typescript-eslint/consistent-type-assertions": [
					"error",
					{
						"assertionStyle": "as",
						"objectLiteralTypeAssertions": "allow"
					}
				],
				"@typescript-eslint/consistent-type-imports": [
					"error",
					{
						"prefer": "type-imports",
						"disallowTypeAnnotations": true
					}
				],
				"@typescript-eslint/explicit-member-accessibility": [
					"error",
					{
						"accessibility": "no-public"
					}
				],
				"@typescript-eslint/member-delimiter-style": [
					"error",
					{
						"multiline": {
							"delimiter": "comma",
							"requireLast": false
						},
						"singleline": {
							"delimiter": "comma",
							"requireLast": false
						}
					}
				],
				"@typescript-eslint/method-signature-style": [
					"error",
					"property"
				],
				"@typescript-eslint/no-confusing-void-expression": [
					"error",
					{
						"ignoreArrowShorthand": false
					}
				],
				"@typescript-eslint/no-extra-non-null-assertion": "error",
				"@typescript-eslint/no-for-in-array": "error",
				"@typescript-eslint/no-inferrable-types": "error",
				"@typescript-eslint/no-misused-new": "error",
				"@typescript-eslint/no-namespace": "error",
				"@typescript-eslint/no-non-null-asserted-optional-chain": "error",
				"@typescript-eslint/no-unnecessary-boolean-literal-compare": [
					"error",
					{
						"allowComparingNullableBooleansToTrue": false,
						"allowComparingNullableBooleansToFalse": true
					}
				],
				"@typescript-eslint/no-unnecessary-condition": [
					"warn",
					{
						"allowConstantLoopConditions": true
					}
				],
				"@typescript-eslint/no-unnecessary-qualifier": "warn",
				"@typescript-eslint/no-unnecessary-type-assertion": "error",
				"@typescript-eslint/no-unnecessary-type-constraint": "error",
				"@typescript-eslint/no-var-requires": "error",
				"@typescript-eslint/non-nullable-type-assertion-style": "warn",
				"@typescript-eslint/prefer-as-const": "error",
				"@typescript-eslint/prefer-function-type": "error",
				"@typescript-eslint/prefer-literal-enum-member": "error",
				"@typescript-eslint/prefer-reduce-type-parameter": "error",
				"@typescript-eslint/prefer-regexp-exec": "error",
				"@typescript-eslint/prefer-string-starts-ends-with": "error",
				"@typescript-eslint/prefer-ts-expect-error": "error",
				"@typescript-eslint/triple-slash-reference": "error",
				"@typescript-eslint/type-annotation-spacing": [
					"error",
					{
						"before": false,
						"after": true,
						"overrides": {
							"arrow": {
								"before": true,
								"after": true
							}
						}
					}
				],
				"@typescript-eslint/unbound-method": [
					"error",
					{
						"ignoreStatic": true
					}
				],
				"@typescript-eslint/unified-signatures": "error",
				"brace-style": "off",
				"@typescript-eslint/brace-style": [
					"error",
					"1tbs",
					{
						"allowSingleLine": false
					}
				],
				"comma-dangle": "off",
				"@typescript-eslint/comma-dangle": [
					"error",
					"never"
				],
				"comma-spacing": "off",
				"@typescript-eslint/comma-spacing": [
					"error",
					{
						"before": false,
						"after": true
					}
				],
				"default-param-last": "off",
				"@typescript-eslint/default-param-last": "error",
				"dot-notation": "off",
				"@typescript-eslint/dot-notation": "error",
				"func-call-spacing": "off",
				"@typescript-eslint/func-call-spacing": [
					"error",
					"never"
				],
				"indent": "off",
				"@typescript-eslint/indent": [
					"warn",
					"tab",
					{
						"SwitchCase": 1
					}
				],
				"keyword-spacing": "off",
				"@typescript-eslint/keyword-spacing": [
					"error",
					{
						"before": true,
						"after": true
					}
				],
				"lines-between-class-members": "off",
				"@typescript-eslint/lines-between-class-members": [
					"error",
					"always"
				],
				"no-array-constructor": "off",
				"@typescript-eslint/no-array-constructor": "error",
				"no-duplicate-imports": "off",
				"@typescript-eslint/no-duplicate-imports": "error",
				"no-extra-semi": "off",
				"@typescript-eslint/no-extra-semi": "error",
				"no-throw-literal": "off",
				"@typescript-eslint/no-throw-literal": "error",
				"no-unused-vars": "off",
				"@typescript-eslint/no-unused-vars": [
					"warn",
					{
						"vars": "all",
						"args": "after-used"
					}
				],
				"no-useless-constructor": "off",
				"@typescript-eslint/no-useless-constructor": "error",
				"quotes": "off",
				"@typescript-eslint/quotes": [
					"error",
					"single",
					{
						"avoidEscape": true
					}
				],
				"semi": "off",
				"@typescript-eslint/semi": [
					"warn",
					"always"
				],
				"space-before-function-paren": "off",
				"@typescript-eslint/space-before-function-paren": [
					"error",
					{
						"anonymous": "always",
						"named": "never",
						"asyncArrow": "always"
					}
				],
				"space-infix-ops": "off",
				"@typescript-eslint/space-infix-ops": "error"
			}
		},
		{
			"files": [
				"*.ts",
				"*.tsx"
			],
			"excludedFiles": [
				"*.d.ts"
			],
			"rules": {
				"@typescript-eslint/consistent-type-definitions": [
					"error",
					"type"
				]
			}
		},
		{
			"files": [
				"*.jsx",
				"*.tsx"
			],
			"rules": {
				"react/jsx-boolean-value": "error",
				"react/jsx-closing-bracket-location": [
					"error",
					"tag-aligned"
				],
				"react/jsx-curly-brace-presence": [
					"error",
					"never"
				],
				"react/jsx-curly-newline": [
					"error",
					{
						"multiline": "consistent",
						"singleline": "forbid"
					}
				],
				"react/jsx-curly-spacing": [
					"error",
					{
						"when": "never"
					}
				],
				"react/jsx-equals-spacing": [
					"error",
					"never"
				],
				"react/jsx-fragments": [
					"error",
					"syntax"
				],
				"react/jsx-indent": [
					"warn",
					"tab",
					{
						"checkAttributes": false,
						"indentLogicalExpressions": true
					}
				],
				"react/jsx-indent-props": [
					"warn",
					"tab"
				],
				"react/jsx-no-bind": "error",
				"react/jsx-no-comment-textnodes": "off",
				"react/jsx-no-constructed-context-values": "warn",
				"react/jsx-no-script-url": "error",
				"react/jsx-props-no-multi-spaces": "error",
				"react/jsx-tag-spacing": [
					"warn",
					{
						"closingSlash": "never",
						"beforeSelfClosing": "always",
						"afterOpening": "never",
						"beforeClosing": "never"
					}
				],
				"react/jsx-wrap-multilines": [
					"error",
					{
						"declaration": "parens-new-line",
						"assignment": "parens-new-line",
						"return": "parens-new-line",
						"arrow": "parens-new-line",
						"condition": "parens-new-line",
						"logical": "parens-new-line",
						"prop": "parens-new-line"
					}
				]
			}
		}
	]
}