import useSWR from 'swr';
import Link from '../Link';
import FlashyTitle from '../FlashyTitle';
import Nav from '../Nav';
import './styles.module.scss';

export type HeaderProps = { noFlashyTitle?: boolean };

const Header = ({ noFlashyTitle }: HeaderProps) => {
	const headers: string[] | undefined = useSWR('/api/footers').data;
	const header = headers && headers[Math.floor(Math.random() * headers.length)];
	
	return (
		<header>
			<div className="mspface-container">
				{header && (
					<style jsx global>{`
						header .mspface {
							background-image: url(/images/footers/${header});
						}
					`}</style>
				)}
				<Link className="mspface" href="/" title="MSPFA Home" tabIndex={-1} draggable={false} />
				<div className="wealth-spawner-container" />
			</div>
			{!noFlashyTitle && <FlashyTitle />}
			<Nav />
		</header>
	);
};

export default Header;