import './styles.module.scss';

const NavDelimiter = () => <span className="nav-delimiter" />;

export default NavDelimiter;